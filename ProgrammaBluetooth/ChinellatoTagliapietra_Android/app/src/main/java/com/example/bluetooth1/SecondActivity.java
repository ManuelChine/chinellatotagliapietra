package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.util.Set;
import java.util.UUID;

import static android.graphics.Color.*;

public class SecondActivity extends AppCompatActivity {
    public static BluetoothAdapter bluetoothAdapter;
    public static String listElement;
    public static BluetoothDevice targetDevice;
    public static BluetoothSocket btSocket;
    public static OutputStreamWriter writer;

    LinearLayout llCerchioRGB;
    SeekBar sbTrasparenzaRed;
    SeekBar SbTrasparenzaGreen;
    SeekBar SbTrasparenzaBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        llCerchioRGB = (LinearLayout) findViewById(R.id.llCerchioRGB);
        sbTrasparenzaRed = (SeekBar) findViewById(R.id.sbTrasparenzaRed);
        sbTrasparenzaRed.setOnSeekBarChangeListener(seekBarRed);
        SbTrasparenzaGreen = (SeekBar) findViewById(R.id.sbTrasparenzaGreen);
        SbTrasparenzaGreen.setOnSeekBarChangeListener(seekBarGreen);
        SbTrasparenzaBlue = (SeekBar) findViewById(R.id.sbTrasparenzaBlue);
        SbTrasparenzaBlue.setOnSeekBarChangeListener(seekBarBlue);
    }

    private OnSeekBarChangeListener seekBarRed = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
           GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
           mostraColori(myCircle);


        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                invia();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };
    private OnSeekBarChangeListener seekBarGreen = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
            mostraColori(myCircle);

        }


        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                invia();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private OnSeekBarChangeListener seekBarBlue = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            GradientDrawable myCircle = (GradientDrawable) llCerchioRGB.getBackground();
            mostraColori(myCircle);

        }


        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            try {
                invia();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    public void mostraColori(GradientDrawable myCircle) {
        int color = Color.rgb(sbTrasparenzaRed.getProgress(), SbTrasparenzaGreen.getProgress(), SbTrasparenzaBlue.getProgress());
        myCircle.setColor(color);
    }

    public void invia() throws IOException {
        int red = sbTrasparenzaRed.getProgress();
        int green = SbTrasparenzaGreen.getProgress();
        int blue = SbTrasparenzaBlue.getProgress();
        String r = Integer.toString(red);
        String g = Integer.toString(green);
        String b = Integer.toString(blue);
        String coloreFinale = "";
        if (red < 100) {
            r = "0" + r;
            if (red < 10) {
                r = "0" + r;
            }
        }
        coloreFinale += r;

        if (green < 100) {
            g = "0" + g;
            if (green < 10) {
                g = "0" + g;
            }
        }
            coloreFinale += g;

        if (blue < 100) {
            b = "0" + b;
            if (blue < 10) {
                b = "0" + b;
            }
        }
            coloreFinale += b;

        writer.write(coloreFinale);
        writer.flush();
    }
}
