#include <SoftwareSerial.h>

const int RXPin = 2;  // da collegare su TX di HC05
const int TXPin = 3;  // da collegare su RX di HC05
const int ritardo = 10;
const int r = 9;
const int g = 10;
const int b = 11;
String colore = "";

//creo una nuova porta seriale via software
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);

char msgChar;

void setup()
{
  pinMode(RXPin, INPUT);
  pinMode(TXPin, OUTPUT);

  myBT.begin(9600);

  Serial.begin(9600);
}

void loop() {
  int i = 0;
  colore = "";
  int red = 0;
  int green = 0;
  int blue = 0;
  while (myBT.available()) {
    msgChar = char(myBT.read());
    Serial.print(msgChar);
    colore += msgChar;
    i++;
    delay(ritardo);
  } if (i != 0) {
    red = colore.substring(0, 3).toInt();
    green = colore.substring(3, 6).toInt();
    blue = colore.substring(6, 9).toInt();
    rgb(red, green, blue);
  }

}

void rgb(int rosso, int verde, int blu) {
  Serial.println("");
  analogWrite(r, rosso);
  Serial.println(rosso);
  analogWrite(g, verde);
  Serial.println(verde);
  analogWrite(b, blu);
  Serial.println(blu);
}
